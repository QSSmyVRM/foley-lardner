﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Xml;

namespace GoogleService
{
    public partial class GoogleService : ServiceBase
    {
        System.Timers.Timer _GoogleChannelTimer = new System.Timers.Timer();
        //ZD 104846 start
        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        static int purgeDuration = 0;
        static System.Timers.Timer timerAutoPurgelogs = new System.Timers.Timer();
        //ZD 104846 End
        public GoogleService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            double _GoogleChannelInterval = 30000;
            double PurgeLogsInterval = 24 * 60 * 60 * 1000; //ZD 104846 //24 Hours
            
            try
            {

                GoogleWatchChannel GoogleWatchChannel = new GoogleWatchChannel();
                GoogleWatchChannel.GoogleChannelupdate();
                _GoogleChannelTimer.Elapsed += new System.Timers.ElapsedEventHandler(GoogleChannelInterval_Elapsed);
                _GoogleChannelTimer.Interval = _GoogleChannelInterval;
                _GoogleChannelTimer.AutoReset = true;
                _GoogleChannelTimer.Start();

                //ZD 104846 start
                timerAutoPurgelogs.Elapsed += new System.Timers.ElapsedEventHandler(timerAutoPurgelogs_Elapsed);
                timerAutoPurgelogs.Interval = PurgeLogsInterval;
                timerAutoPurgelogs.Enabled = true;
                timerAutoPurgelogs.Start();
                //ZD 104846 End

            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnStop()
        {
            _GoogleChannelTimer.Enabled = false;
            _GoogleChannelTimer.AutoReset = false;
            _GoogleChannelTimer.Stop();
        }
        #region GoogleChannelInterval_Elapsed
        /// <summary>
        /// GoogleChannelInterval_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoogleChannelInterval_Elapsed(object sender, EventArgs e)
        {
            double GoogleChannelInterval = 1 * 60 * 1000;
            try
            {
                _GoogleChannelTimer.Stop();
                GoogleWatchChannel GoogleWatchChannel = new GoogleWatchChannel();
                GoogleWatchChannel.GoogleChannelupdate();

                _GoogleChannelTimer.Interval = GoogleChannelInterval;
                _GoogleChannelTimer.AutoReset = true;
                _GoogleChannelTimer.Start();

            }
            catch (Exception)
            {
            }


        }
        #endregion

        //ZD 104846 start
        #region timerAutoPurgelogs_Elapsed
        /// <summary>
        /// timerAutoPurgelogs_Elapsed
        /// </summary>
        void timerAutoPurgelogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                timerAutoPurgelogs.AutoReset = false;
                GetSitePurgeLogDuartion();
                PurgeLogs();
                System.Threading.Thread.Sleep(5000);
                timerAutoPurgelogs.AutoReset = true; ;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region GetSitePurgeLogDuartion
        /// <summary>
        /// GetSitePurgeLogDuartion()
        /// </summary>
        private void GetSitePurgeLogDuartion()
        {
            XmlDocument xmldoc = new XmlDocument();
            string stmt = "", schemapath = "";
            try
            {
                schemapath = "C:\\VRMSchemas_v1.8.3\\";
                ns_SqlHelper.SqlHelper sqlCon = new ns_SqlHelper.SqlHelper(schemapath);
                sqlCon.OpenConnection();
                stmt = "select AutoPurgeLogDuration from Sys_Settings_D";
                System.Data.DataSet ds = sqlCon.ExecuteDataSet(stmt);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        int.TryParse(ds.Tables[0].Rows[0]["AutoPurgeLogDuration"].ToString(), out purgeDuration);
                    }
                }
                sqlCon.CloseConnection();
            }

            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region PurgeLogs
        /// <summary>
        /// PurgeLogs
        /// </summary>
        private void PurgeLogs()
        {
            string[] Files = null;
            try
            {
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\MaintenanceLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\MaintenanceLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();

                    }
                }
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\RTCLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\RTCLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        //ZD 104846 End
    }
}
