/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

namespace ns_EmailListMain
{
    public partial class en_EmailList2main : System.Web.UI.Page
    {

        #region Protected Data members

        protected System.Web.UI.WebControls.Label LblError;
        protected System.Web.UI.WebControls.Label LblHeading;
        protected System.Web.UI.HtmlControls.HtmlInputHidden fromSearch;
        protected System.Web.UI.HtmlControls.HtmlInputHidden FirstName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden LastName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden LoginName;

        #endregion

        #region Private and Public variables

        protected string sortBy = "";
        protected string alphabet = "";
        protected string pageNo = "";
        protected string titlealign = "";
        ns_Logger.Logger log = new ns_Logger.Logger();//ZD 100263
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
        #endregion

        #region Page Load  Event Hander
        protected void Page_Load(object sender, EventArgs e)
        {
            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions(); //ZD 100263
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("emaillist2main.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

               
                
                GetQueryStrings();

                if (Request.QueryString["t"] == "g")
                    LblHeading.Text = obj.GetTranslatedText("Guest Address Book");//FB 1830 - Translation
                else if (Application["Client"].ToString().ToUpper() == "DISNEY") //FB 1985
                    LblHeading.Text = "Address Book"; 
                else
                    LblHeading.Text = obj.GetTranslatedText("Address Book");//FB 1830 - Translation


            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                LblError.Text = obj.ShowSystemMessage();
                LblError.Visible = true;
                //LblError.Text = "PageLoad: " + ex.Message;
            }
        }
        #endregion

        #region GetQueryStrings

        /// <summary>
        /// User Defined Method - Building GetQueryStrings Structure
        /// </summary>

        private void GetQueryStrings()
        {

            try
            {
                if (Request.QueryString["srch"] != null)
                {
                    if (Request.QueryString["srch"].ToString().Trim() != "")
                    {

                        fromSearch.Value = Request.QueryString["srch"].ToString().Trim();
                    }
                }
                if (Request.QueryString["fname"] != null)
                {
                    if (Request.QueryString["fname"].ToString().Trim() != "")
                    {

                        FirstName.Value = Request.QueryString["fname"].ToString().Trim();
                    }
                }

                if (Request.QueryString["lname"] != null)
                {
                    if (Request.QueryString["lname"].ToString().Trim() != "")
                    {

                        LastName.Value = Request.QueryString["lname"].ToString().Trim();
                    }
                }
                if (Request.QueryString["gname"] != null)
                {
                    if (Request.QueryString["gname"].ToString().Trim() != "")
                    {

                        LoginName.Value = Request.QueryString["gname"].ToString().Trim();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        #endregion

        #region GoToSearch
        protected void GoToSearch(Object sender, EventArgs e)
        {
            //ZDLatestIssue-Hide View Guest Starts
            string fn = "";
            string n = "", chk = "";
            try
            {
                if (Request.QueryString["fn"] != null)
                    fn = Request.QueryString["fn"].ToString();

                if (Request.QueryString["n"] != null)
                    n = Request.QueryString["n"].ToString();

                //ZDLatestIssue-Hide View Guest Starts
                if (Request.QueryString["chk"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["chk"].ToString()))
                    chk = "&chk=" + Request.QueryString["chk"].ToString();

                string url = "emailsearch.aspx?t=" + Request.QueryString["t"].ToString() + "&frm=" + Request.QueryString["frm"].ToString() + chk + "&fn=" + fn + "&n=" + n;
                //ZDLatestIssue-Hide View Guest End
                Response.Redirect(url, true);
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {}
        }
        #endregion
    }
}