﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Web.Security;
using System.Web.Util;
using System.Collections;
using System.Web.UI.WebControls.WebParts;
using System.Text.RegularExpressions;



namespace myVRMNet
{
    public class RSSXMLGenerator 
    {
       #region Private Data Members
        /// <summary>
        /// Data Members
        /// </summary>

        private string userID = "";
        String ConfigPath = "C:\\VRMSchemas_v1.8.3\\";
        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        public ns_Logger.Logger log;
        protected String language = "";//FB 1830
        string Filtertype = "";//FB 2639 - Search
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public RSSXMLGenerator()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();

            
        }
        public RSSXMLGenerator(String tickertxt)
        {
            HttpContext.Current.Session.Add("ticker", tickertxt);
        }
        #endregion
        
        #region Search Conference OutXML

        public String SearchConferenceOutXML(String userId,String Cmd,String type, String orgelement) //organization module
        {
            String outXML = "";
            String inXML = "";

            try
            {
               userID = userId;
               inXML = SearchConferenceInXML(Cmd,type, orgelement); //organization module
               //if (type.Trim() == "")
               //{
               //    outXML = obj.CallMyVRMServer("SearchConference", inXML, ConfigPath);
               //}
               //else
               //{
                   outXML = obj.CallMyVRMServer("SearchAllConference", inXML, ConfigPath);
               //}
            }
            catch(Exception ex)
            {
                log.Trace(ex.Message);
            }
            return outXML;
        }
        #endregion

        #region Ticker Generator

        public String TickerGenerator(String userId,String tickertype,String dateFormat,String timeFormat)
        {
            String outXML = "";
            String inXML = "";
            String rssTicker = "";
            string webURL = "";
            string isrecur = "";
            string confid = "";
            string SortingOrder = "0";//FB 2822
            try
            {
                userID = userId;

                if (timeFormat.ToString() == "0")
                    timeFormat = "HH:mm";
                else
                    timeFormat = "hh:mm tt";

                String countInXML = "";
                countInXML += "<ConferenceStatus>";
                countInXML += "  <path>" + ConfigPath + "</path>";
                countInXML += "  <userID>" + userId + "</userID>";
                countInXML += "</ConferenceStatus>";

                string imagepath = "image/tickerText.jpg";

                //string confXMLCount = obj.CallMyVRMServer("GetConferenceStatusCount", countInXML, ConfigPath);

                //XmlDocument xmldoc1 = new XmlDocument();
                //xmldoc1.LoadXml(confXMLCount);
                //XmlNodeList nodescount = xmldoc1.SelectNodes("//ConferenceStatus");
                //String confCount = " " + "<img  src=" + imagepath + " />" + " " + "<b>" + nodescount.Item(0).SelectSingleNode("ongoing").InnerText + " " + "Ongoing Conference(s)" + "</b>";
                //confCount += " " + "<img  src=" + imagepath + " />" + " " + "<b>" + nodescount.Item(0).SelectSingleNode("scheduled").InnerText + " " + "Scheduled Conference(s)" + "</b>";
                //confCount += " " + "<img  src=" + imagepath + " />" + " " + "<b>" + nodescount.Item(0).SelectSingleNode("pending").InnerText + " " + "Conference(s) Pending for Approval" + "</b>";

                //rssTicker = confCount;

                //FB 1830- Starts                
                if (HttpContext.Current.Session["language"] == null)
                    HttpContext.Current.Session["language"] = "en";

                if (HttpContext.Current.Session["language"].ToString() != "")
                    language = HttpContext.Current.Session["language"].ToString();
                //FB 1830- End

                if (tickertype.ToString().Trim() == "0")
                {
                    string ConfStatus = "0,7,3,1,5,6";
                    DateTime datefrom = DateTime.Today;
                    String dateFrom = Convert.ToString(datefrom);
                    DateTime dateto = datefrom.AddDays(45);
                    String dateTo = Convert.ToString(dateto);
                    Filtertype = ns_MyVRMNet.vrmSearchFilterType.custom;//FB 2639 - Search
                    inXML = objInXML.SearchConference(userId, ConfStatus, "", "", "", "", "5", dateFrom, dateTo, "", "", "", "1", "", "1", "3", "0", "", "", "", SortingOrder, "", false, Filtertype,""); //FB 2632//FB 2694//FB 2822 //FB 2870 //FB 3006 TIK# 100037 //FB 2639 - Search
                    outXML = obj.CallMyVRMServer("SearchConference", inXML, ConfigPath);

                    if (outXML.IndexOf("<error>") <= 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                        webURL = GetSysSettingDetails(ConfigPath);
                        
                        if (nodes.Count > 0)
                        {
                            String confName = nodes.Item(0).SelectSingleNode("ConferenceID").InnerText;
                            foreach (XmlNode node in nodes)
                            {

                                if (node.SelectSingleNode("IsRecur") != null)
                                    if (node.SelectSingleNode("IsRecur").InnerText != "")
                                        isrecur = node.SelectSingleNode("IsRecur").InnerText;

                                if (isrecur == "1")
                                {
                                    if (node.SelectSingleNode("ConferenceID") != null)
                                        if (node.SelectSingleNode("ConferenceID").InnerText != "")
                                            confid = node.SelectSingleNode("ConferenceID").InnerText;

                                    if (confid.IndexOf(",") > 0)
                                    {
                                        String[] confID = confid.Split(',');
                                        confid = confID[0];
                                    }
                                }
                                else
                                {
                                    if (node.SelectSingleNode("ConferenceID") != null)
                                        if (node.SelectSingleNode("ConferenceID").InnerText != "")
                                            confid = node.SelectSingleNode("ConferenceID").InnerText;
                                }

                               // nodes.Item(0).SelectSingleNode("ConferenceDateTime").InnerText

                                DateTime date1 = Convert.ToDateTime(nodes.Item(0).SelectSingleNode("ConferenceDateTime").InnerText);
                                
                                String confDate1 = date1.ToString(dateFormat);
                                String confTime1 = date1.ToString(timeFormat);
                                String currentDate = DateTime.Today.ToString(dateFormat);
                               
                                if (confDate1 == currentDate)
                                    confDate1 = obj.GetTranslatedText("Today"); //ZD 100288

                                String confdatetime1 = confDate1 + " ," + confTime1;

                                DateTime date = Convert.ToDateTime(node.SelectSingleNode("ConferenceDateTime").InnerText);
                                String confDate = date.ToString(dateFormat);
                                String confTime = date.ToString(timeFormat);

                                String currentDate1 = DateTime.Today.ToString(dateFormat);
                                if (confDate == currentDate)
                                    confDate = obj.GetTranslatedText("Today"); //ZD 100288

                                String confdatetime = confDate + " ," + confTime;
                                String rssurl = "";
                                if (webURL.Contains("genlogin.aspx")) //Login Management
                                {
                                    rssurl = webURL + "?rss=" + confid;
                                }
                                else
                                {
                                    rssurl = webURL + "/" + language + "/genlogin.aspx?rss=" + confid;//Login Management //FB 1830
                                }

                                if (confName != node.SelectSingleNode("ConferenceID").InnerText)
                                {
                                    rssTicker += " " + "<img  src=" + imagepath + " />" + " " + "<a href=" + rssurl + " title=click to view the conference details>" + node.SelectSingleNode("ConferenceName").InnerText + "-" + confdatetime + "</a>";
                                }
                                else
                                {
                                    rssTicker += " " + "<img  src=" + imagepath + " />" + " " + "<a href=" + rssurl + ">" + node.SelectSingleNode("ConferenceName").InnerText + " - " + confdatetime1 + "</a>";
                                }
                            }


                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return rssTicker;
        }
        #endregion

        #region FeedTicker Generator
        public String FeedTickerGenerator(String feedurl,String userId,String feedLink)
        {
            String rssTicker = "";
            try
            {
                XmlDocument xmlDocument = null;
                xmlDocument = new XmlDocument();
                if (feedurl.Contains("RSSGenerator.aspx") && !feedurl.Contains("m=p"))
                {
                    feedurl = feedurl + "?u=" + userId;
                }
                xmlDocument.Load(feedurl);
                
                XmlNodeList nodes = xmlDocument.SelectNodes("//rss/channel/item");
                XmlNodeList nodes1 = xmlDocument.SelectNodes("//rss/channel");
                 string imagepath = "image/tickerText.jpg";
                 String RSSTitle = nodes1.Item(0).SelectSingleNode("title").InnerText;

                 if (feedLink.Trim() == "0")
                     HttpContext.Current.Session.Add("RSSTitle", RSSTitle);

                 if (feedLink.Trim() == "1")
                     HttpContext.Current.Session.Add("RSSTitle1", RSSTitle);

                if (nodes.Count > 0)
                {
                    String rsstitle = nodes.Item(0).SelectSingleNode("title").InnerText;
                    String rsslink = nodes.Item(0).SelectSingleNode("link").InnerText;
                    rssTicker = " " + "<img  src=" + imagepath + " />" + " " + "<a href=" + rsslink + " target=_blank>" + rsstitle + "</a>";
                    foreach (XmlNode node in nodes)
                    {
                        
                        String rssurl = node.SelectSingleNode("link").InnerText;
                        if (rsstitle != node.SelectSingleNode("title").InnerText)
                        {
                           // rssTicker += "  " +  "<img  src=" + imagepath +" />" + "  " + node.SelectSingleNode("title").InnerText + " ";
                            rssTicker += "  " +  "<img  src=" + imagepath +" />" + "  " + "<a href=" + rssurl + " target=_blank>" + node.SelectSingleNode("title").InnerText +  "</a>";
                        }
                    }

                }

                  

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return rssTicker;
        }
        #endregion

        #region Search Conference InXML
        /// <summary>
        ///   Search Conference InXML
        /// </summary>
        /// <returns>inputXML</returns>
        public String SearchConferenceInXML(String Cmdid,string type, string orgelement)
        {
            String inputXML = "";
            String strCommand = "";
            Int32 intIndex = 0;
            try
            {
                switch (Cmdid)
                {
                    case "Ongoing": //Ongoing
                        strCommand = "0,6,5";
                        intIndex = 1;
                        break;

                    case "Future":  //Future
                        strCommand = "0,6";
                        break;

                    case "Public":  //Public
                        strCommand = "0,6";
                        intIndex = 3;
                        break;

                    case "Pending":  //Pending
                        strCommand = "1";
                        break;

                    default: //Ongoing
                        strCommand = "0,6,5";
                        intIndex = 1;
                        break;

                }

                inputXML = "<SearchConference>";
                inputXML += orgelement;
                inputXML += "<UserID>" + userID + "</UserID>";
                inputXML += "<ConferenceID></ConferenceID><ConferenceUniqueID></ConferenceUniqueID>";
                //Scheduled = 0, Pending = 1, Terminated = 3, Ongoing = 5, OnMCU = 6, Completed = 7, Deleted = 9, BLANK = ERROR   -->
                inputXML += "<StatusFilter>";
                string[] strTokenArray = strCommand.Split(',');
                foreach (string strToken in strTokenArray)
                {
                    if (strToken.Length > 0)
                        inputXML += "<ConferenceStatus>" + strToken + "</ConferenceStatus>";
                }
                inputXML += "</StatusFilter>";
                inputXML += "<ConferenceName></ConferenceName>";
               

                //1:Ongoing, 2:Today, 3:This week, 4:This month, 5:Cutom, 6:Yesterday, 7:Tomorrow   -->

                inputXML += "<ConferenceSearchType>";
                if (type.Trim() == "" && intIndex != 1)
                {
                    inputXML += "5";
                }
                if (intIndex == 1)
                {
                    inputXML += "1";
                }
                if (intIndex == 2)
                {
                    inputXML += "";
                }


                inputXML += "</ConferenceSearchType>";
                //<!--  In case of ConferenceSearchType 5, these from and to dates MUST be specified   
                inputXML += "<ConferenceHost></ConferenceHost><ConferenceParticipant></ConferenceParticipant>";
                //<!--  1:Public, 0:Private, NONE: Any  
                if (intIndex == 3)// Public 
                    inputXML += "<Public>1</Public>";
                else
                    inputXML += "<Public></Public>";
                //<!-- Use 0  for best results  
                inputXML += "<ApprovalPending>0</ApprovalPending>";

                String dateTo = "";
                String dateFrom = "";

                if (type.Trim() != "P")
                {
                    DateTime datefrom = DateTime.Today;
                     dateFrom = Convert.ToString(datefrom);
                    DateTime dateto = datefrom.AddDays(1);
                     dateTo = Convert.ToString(dateto);
                }

                inputXML += "<DateFrom>"+ dateFrom+"</DateFrom>";
                inputXML += "<DateTo>" + dateTo + "</DateTo>";
                inputXML += "<RecurrenceStyle>1</RecurrenceStyle>";
                inputXML += "<Location>";
                inputXML += "<SelectionType>1</SelectionType>";
                //<!--  0:None, 1:Any, 2:Selected 
                inputXML += "<SelectedRooms></SelectedRooms>";
                inputXML += "</Location>";
                //Page number will be any number based on what is returned in total pages, the first call would have 
                // a value 1
                inputXML += "<PageNo>1</PageNo>";
                //1:UniqueID, 2:Conference Name, 3:Conference Start Date/Time   
                inputXML += "<SortBy>3</SortBy>";
                inputXML += "</SearchConference>";
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return inputXML;
        }

    #endregion

        #region GetSysSettingDetails

        public String GetSysSettingDetails(string myvrmser)
        {
            XmlNodeList nodes = null;
            string webSiteUrl = "";
            try
            {
                string inXML = "";
                string outXML = obj.CallMyVRMServer("GetSysMailData", inXML, myvrmser);

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                nodes = xmldoc.SelectNodes("//GetSysMailData");

                webSiteUrl = nodes.Item(0).SelectSingleNode("WebSiteURL").InnerText;

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }

            return webSiteUrl;
        }
    #endregion

    }
}
