﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_BridgeList.BridgeList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <title>MCU List</title>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />

    <script type="text/javascript">
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

    <script type="text/javascript" src="inc/functions.js"></script>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/MonitorMCU.js"></script>

    <script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>

    <script language="JavaScript">
        function PopupWindow() {
            var popwin = window.showModalDialog("BridgeList.aspx", document, "resizable: yes; scrollbar: no; help: yes; status:yes; dialogHeight:580px; dialogWidth:510px");
        }

        function SelectOneDefault(obj) {
            var i = 1;
            if (navigator.userAgent.indexOf('Trident') > -1)
                i = 0;
            if(window.parent.document.getElementById("txtSelectedMCU") != null) //ZD 100718
                window.parent.document.getElementById("txtSelectedMCU").value = obj.parentNode.parentNode.childNodes[i].innerHTML; //Diff for Chrome and IE
            if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) {
                var elements = document.getElementsByTagName('input');

                for (i = 0; i < elements.length; i++)
                    if ((elements.item(i).type == "radio")) {
                    if (elements.item(i).id != obj.id) {
                        elements.item(i).checked = false;
                    }
                }
            }
        }  
    //ZD 100718 Start
        function ChangeMCU(args) {
        var prnt = window.parent.document.getElementById("locstrname");
        var hdNm = document.getElementById("locstr");
        if (prnt)
            prnt.value = hdNm.value;
   
          window.parent.fnTriggerFromPopup(args);
    }
    //ZD 100718 End
    </script>

</head>
<body>
    <input name="locstr" type="hidden" id="locstr" runat="server" /><%--ZD 100718--%> 
    <form id="frmMCUList" runat="server" class="tabContents">
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <table width="95%" border="0">
                        <tr>
                            <td align="center">
                                <h3>
                                    <asp:Label ID="lblHeader" Text="<%$ Resources:WebResources, SearchBridgeConference_MCUList%>" runat="server"></asp:Label>
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:DataGrid ID="dgMCUs" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                    GridLines="None" BorderColor="blue" Style="border-collapse: separate" BorderStyle="solid"
                                    BorderWidth="1" ShowFooter="true" Width="100%" Visible="true">
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <FooterStyle CssClass="tableBody" />
                                    <HeaderStyle CssClass="tableHeader" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="false">
                                            <HeaderStyle CssClass="tableHeader" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="name" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, SearchBridgeConference_Name%>" ItemStyle-CssClass="tableBody">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="interfaceType" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, InterfaceType%>" ItemStyle-CssClass="tableBody">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="administrator" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Administrator%>" ItemStyle-CssClass="tableBody">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="exist" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, ExistingVirtual%>" ItemStyle-CssClass="tableBody">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="status" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, Status%>" ItemStyle-CssClass="tableBody">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="PollStatus" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, PollStatus%>" ItemStyle-Font-Bold="true" ItemStyle-CssClass="tableBody">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="order" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Select%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="15%">
                                            <HeaderStyle CssClass="tableHeader" />
                                            <ItemTemplate>
                                                <asp:RadioButton ID="rdSelectMCU" onclick="javascript:SelectOneDefault(this);" GroupName="Default"
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Table runat="server" ID="tblNoMCUs" Visible="false" Width="90%">
                                    <asp:TableRow CssClass="lblError">
                                        <asp:TableCell CssClass="lblError">
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, NoMCUsfound%>" runat="server"></asp:Literal>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr align="right">
                            <td>
                                <%--<input name="btnClose" validationgroup="Uploadd" id="btnClose" type="button" class="altMedium0BlueButtonFormat"
                                    runat="server" value="Close" />
                                <input name="btnSubmit" validationgroup="Uploadd" id="btnSubmit" type="button" class="altMedium0BlueButtonFormat"
                                    runat="server" value="Submit" onserverclick="SubmitMCUSelection" />--%>
                                
                                <button name="btnClose" validationgroup="Uploadd" id="btnClose" type="button" class="altMedium0BlueButtonFormat" runat="server"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal></button>&nbsp;&nbsp;
                                <button name="btnSubmit" validationgroup="Uploadd" id="btnSubmit" type="button" class="altMedium0BlueButtonFormat"
                                    runat="server" onserverclick="SubmitMCUSelection"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="center">
                                <asp:DropDownList ID="lstBridgeType" DataTextField="name" DataValueField="ID" runat="server">
                                </asp:DropDownList>
                                <asp:DropDownList ID="lstBridgeStatus" DataTextField="name" DataValueField="ID" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript" src="inc/softedge.js"></script>

</body>
</html>

<script type="text/javascript">
 //ZD 100718 Starts
    function fnClosePopup() {
        $('#popupdiv', window.parent.document).hide();
        $('#PopupMCUList', window.parent.document).hide();
        window.parent.document.getElementById('MCUList').src = "";
        return false;
    }

    $(document).ready(function() {

        $('#btnSubmit').click(function() {
            $('#popupdiv', window.parent.document).hide();
            $('#PopupMCUList', window.parent.document).hide();
            return false;
        });

        $('#btnClose').click(function() {
            fnClosePopup();
        });

    });
    
    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            fnClosePopup();
            return true;
        }
    } 
 //ZD 100718 End
</script>

