<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.LocationList" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void lstRoomSelection_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
</script>

<script type="text/javascript" src="script/DisplayDIV.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<html>
<head>
<%--FB 2790 Starts--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
<%--FB 2790 Ends--%>
    <script language="javascript">

    </script>

</head>
<body>
    <form id="frmLocationList" runat="server" autocomplete="off" method="post" onsubmit="return true"><%--ZD 101190--%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <div>
            <table width="100%" id="tblMain">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, LocationList_SelectaLocati%>" runat="server"></asp:Literal>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="errLabel" runat="server" Font-Size="Small" ForeColor="Red" Visible="False"
                            Font-Bold="True"></asp:Label>
                        <asp:TextBox ID="txtRoomID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:RadioButtonList ID="rdSelView" runat="server" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                            RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                            <asp:ListItem Value="1" Selected="True" Text="<%$ Resources:WebResources, LevelView%>"></asp:ListItem>
                            <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ListView%>"></asp:ListItem>
                        </asp:RadioButtonList><br />
                        <asp:Panel ID="pnlLevelView" runat="server" Height="550" Width="100%" ScrollBars="Auto"
                            BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                            <asp:TreeView ID="treeRoomSelection" runat="server" Height="90%" ShowCheckBoxes="Leaf"
                                ShowLines="True" Width="95%" onclick="javascript:getOneRoom(event)">
                                <NodeStyle CssClass="treeNode" />
                                <RootNodeStyle CssClass="treeRootNode" />
                                <SelectedNodeStyle CssClass="treeSelectedNode" />
                                <ParentNodeStyle CssClass="treeParentNode" />
                                <LeafNodeStyle CssClass="treeLeafNode" />
                            </asp:TreeView>
                        </asp:Panel>
                        <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Height="500" ScrollBars="Auto" Visible="False" Width="100%"
                            HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="Verdana"
                            Font-Size="Small" ForeColor="Green">
                            <asp:RadioButtonList ID="lstRoomSelection" runat="server" Height="95%" Width="95%"
                                Font-Size="Smaller" ForeColor="ForestGreen" Font-Names="Verdana" RepeatLayout="Flow"
                                OnSelectedIndexChanged="lstRoomSelection_SelectedIndexChanged">
                            </asp:RadioButtonList>
                        </asp:Panel>
                        <%--Added for Location Issues  - Start--%>
                        <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                            Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left"
                            Direction="LeftToRight" Font-Size="Small">
                            <table>
                                <tr align="center">
                                    <td>
                                        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, LocationList_YouhavenoRoo%>" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%--Added for Location Issues  - End--%>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnSubmit" OnClick="SetRoom" CssClass="altShortBlueButtonFormat"
                            Text="<%$ Resources:WebResources, LocationList_btnSubmit%>" runat="server" />
                            <asp:Button ID="btnClose" OnClientClick="window.close();" CssClass="altShortBlueButtonFormat"
                            Text="<%$ Resources:WebResources, LocationList_btnClose%>" runat="server" Visible="false" />
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>

        <script language="javascript">
        window.resizeTo(500, 700);
        </script>

    </form>
</body>
</html>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>