<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.Group" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>


<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End	
function OpenDetails(groupID)
{
 // alert(groupID);
  url = "MemberAllStatus.aspx?";
  url += "GroupID=" + groupID;
    
	window.open(url,"","left=50,top=50,width=400,height=250,resizable=yes,scrollbars=yes,status=no");
   return false;

}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End


</script>

  <div id="tblViewDetails" style="display:none">
  </div>
  
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
    <title>My Groups</title>
</head>
<body >
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
     <input type="hidden" id="helpPage" value="73">
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, ManageGroup_lblHeader%>"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 100%"> <%--FB 2921--%>
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr><div id="dataLoadingDIV" style="display:none" align="center" >
                    <img border='0' src='image/wait1.gif'  alt='Loading..' />
                 </div> <%--ZD 100678 End--%>
            </tr><%--ZD 100176--%>
               <tr>
                <td align="center">
                    <asp:DataGrid ID="dgGroups" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" OnSortCommand="SortGroups"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="False" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteGroup" OnEditCommand="EditGroup" Width="70%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                          <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle cssclass="tableBody" />
                        <Columns>
                            <asp:BoundColumn datafield="groupID" visible="false"><HeaderStyle cssclass="tableHeader"></HeaderStyle></asp:BoundColumn>
                            <asp:BoundColumn datafield="groupName" itemstyle-cssclass="tableBody" headerstyle-cssclass="tableHeader" headertext="<%$ Resources:WebResources, ManageGroup_GroupName%>" sortexpression="1" headerstyle-horizontalalign="Left" itemstyle-horizontalalign="Left"></asp:BoundColumn> <%-- FB 2050 --%>
                            <asp:BoundColumn datafield="description" itemstyle-cssclass="tableBody" headerstyle-cssclass="tableHeader" headertext="<%$ Resources:WebResources, ManageGroup_Description%>" headerstyle-horizontalalign="Center" itemstyle-horizontalalign="Center"></asp:BoundColumn><%-- FB 2921--%>
                            <asp:BoundColumn datafield="ownerName" itemstyle-cssclass="tableBody" headerstyle-cssclass="tableHeader" headertext="<%$ Resources:WebResources, ManageGroup_Owner%>" sortexpression="2" headerstyle-horizontalalign="Center" itemstyle-horizontalalign="Center"></asp:BoundColumn><%-- FB 2921--%>
                            <asp:BoundColumn datafield="public" itemstyle-cssclass="tableBody" headerstyle-cssclass="tableHeader" headertext="<%$ Resources:WebResources, ManageGroup_PrivatePublic%>" sortexpression="3" headerstyle-horizontalalign="Center" itemstyle-horizontalalign="Center"></asp:BoundColumn><%-- FB 2921--%>
                            <asp:TemplateColumn headertext="<%$ Resources:WebResources, ManageGroup_ViewMembers%>" itemstyle-cssclass="tableBody" headerstyle-cssclass="tableHeader" headerstyle-horizontalalign="Center" itemstyle-horizontalalign="Center"><%-- FB 2921--%>
                                <ItemTemplate>
                                    <%--<asp:Button id="btnViewDetails" onfocus="this.blur()" text="<%$ Resources:WebResources, ManageGroup_btnViewDetails%>" runat="server" cssclass="altMedium0BlueButtonFormat"></asp:Button>--%>
                                    <button id="btnViewDetails" runat="server" class="altMedium0BlueButtonFormat" ><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_btnViewDetails%>" runat="server"></asp:Literal></button>
                               </ItemTemplate>
                             </asp:TemplateColumn>
                              <asp:TemplateColumn headertext="<%$ Resources:WebResources, ManageGroup_Actions%>" headerstyle-horizontalalign="center">
                                <HeaderStyle cssclass="tableHeader"></HeaderStyle>
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td align="center"><%-- FB 2921--%>
                                                <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageGroup_btnEdit%>" id="btnEdit" commandname="Edit"></asp:LinkButton>
                                            </td>
                                            <td align="center"><%-- FB 2921--%>
                                                <asp:LinkButton runat="server" text="<%$ Resources:WebResources, ManageGroup_btnDelete%>" id="btnDelete" commandname="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" id="tblNoGroups" visible="false" width="90%">
                        <asp:TableRow cssclass="lblError">
                            <asp:TableCell cssclass="lblError" horizontalalign="center">
                               <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ManageGroup_lblError%>" runat="server"></asp:Literal>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>   
                </td>
            </tr>
             <tr>
                <td height="50" colspan="3" align="center" valign="middle">
                <br />
                  <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2" style="vertical-align:middle" /> <%--FB 2921--%>
                  <font color="#00CCFF" size="3"><b><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_OR%>" runat="server"></asp:Literal></b></font>
                  <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2" style="vertical-align:middle" /> <%--FB 2921--%>
		        </td>
           </tr>
           
            <tr>
                <td>
                    <table cellspacing="5" width="100%">
                        <tr>
                            <td width="170"  align="lef">&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_SearchGroupsa%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px">
                    <table width="65%" align="center"> 
                        <tr>
                            <td align="center">
                                <table width="100%" align="center">
                                    <tr>
                                        <td align="left" class="blackblodtext" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_GroupName%>" runat="server"></asp:Literal></td><%-- FB 2921--%>
                                        <td align="left" width="350%">
                                            <asp:TextBox ID="txtSGroupName" runat="server" CssClass="altText" style="margin-left:0px"></asp:TextBox> <%--FB 2921--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSGroupName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        
                                        <td align="left" class="blackblodtext" nowrap=""><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_IncludedMember%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSMember" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSMember" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_Description%>" runat="server"></asp:Literal></td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" runat="server" CssClass="altText" style="Width:200px;Height:36px;"></asp:TextBox> <%--FB 2921--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"> <%--ZD 100420--%>
                            <button id="btnReset" runat="server" onserverclick="ResetGroup" class="altMedium0BlueButtonFormat" onclick="javascript:DataLoading('1');" ><asp:Literal Text="<%$ Resources:WebResources, Reset%>" runat="server"></asp:Literal></button>
                            <button id="btnSubmit" runat="server" onserverclick="SearchGroup" class="altMedium0BlueButtonFormat" onclick="javascript:DataLoading('1');" ><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_btnSubmit%>" runat="server"></asp:Literal></button>
                             <%--<asp:Button ID="btnReset" onfocus="this.blur()" OnClick="ResetGroup" runat="server" CssClass="altLongBlueButtonFormat" Text="Reset" Width="15%" OnClientClick="javascript:DataLoading('1');" />--%><%--ZD 100176--%>
                             <%--<asp:Button ID="btnSubmit" onfocus="this.blur()" OnClick="SearchGroup" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit"  Width="15%" OnClientClick="javascript:DataLoading('1');"/>--%><%--ZD 100176--%>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="50" colspan="3" align="center" valign="middle">
                <br />
                  <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2" style="vertical-align:middle" /> <%--FB 2921--%>
                  <font color="#00CCFF" size="3"><b><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_OR%>" runat="server"></asp:Literal></b></font>
                  <img border="0" src="../image/aqualine.gif" alt="aqualine" width="200" height="2" style="vertical-align:middle" /> <%-- FB 2921--%>
		        </td>
           </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5" width="70%">
                        <tr>
                            <td width="170" align="left">&nbsp;</td>
                            <td>
                               <%-- <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_CreateNewGrou%>" runat="server"></asp:Literal></SPAN>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="center" style="width: 1168px"> <%--FB 2921--%>
                    <table  width="65%"> <%--FB 2921--%>
                        <tr>
                            <td align="right"> <%--ZD 100420--%>
                            <button id="btnCreate" runat="server" onserverclick="CreateNewGroup" class="altMedium0BlueButtonFormat" style="width:25%;" ><asp:Literal Text="<%$ Resources:WebResources, ManageGroup_CreateNewGrou%>" runat="server"></asp:Literal></button> <%--ZD 100926--%>
                            <%--<asp:Button ID="btnCreate1" onfocus="this.blur()" OnClick="CreateNewGroup" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit" style="width:15%;"/>--%> <%--FB 2921--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
    
