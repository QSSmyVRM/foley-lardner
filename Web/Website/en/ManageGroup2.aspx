<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageGroup2"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script>
<script src="../i18n/i18Utils.js" type="text/javascript" ></script>

<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script> <%--Login Management--%>
<script type="text/javascript" src="script/group2.js"></script>


<script type="text/javascript" language="javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
	function frmManagegroup2_Validator()
    {	
        var groupname = document.getElementById('<%=GroupName.ClientID%>');
        if( groupname.value == "")
        {
            var txtReqFieldGName = document.getElementById('<%=ReqFieldGName.ClientID%>');
            txtReqFieldGName.style.visibility = 'visible';
            groupname.focus();
            return false;
        }
        else if (groupname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            regGName.style.display = 'block';
            groupname.focus();
            return false;
        }           
	    // chk group number
	    if(document.frmManagegroup2.PartysInfo.value == "")  //EDITED for FF
	    {
		    alert(GroupMinAlert);
		    return false;
	    }
	    else
	    {
	        if(document.frmManagegroup2.PartysInfo.value == "|") //FB 1914
	        {
		        alert(GroupMinAlert);
		        return false;
	        }
	    }
	    if(chkGroupExists(document.frmManagegroup2.GroupName.value) == true) 
	    {	
		    alert(GroupDuplicate);
		    document.frmManagegroup2.GroupName.focus();
		    return false;
	    }	    
    	
    	var grpDesc = document.getElementById('<%=GroupDescription.ClientID%>');
	    if (Trim(grpDesc.value) != "")
	    {
	        if (grpDesc.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/))
            {
                regGrpDescription.style.display = 'block';
                grpDesc.focus();
                return false;
            }
	    }
    	
	    // refresh the party list and calculate the party number
	    willContinue = ifrmMemberlist.bfrRefresh(); 
	    if (!willContinue)
		    return false;
        DataLoading(1); // ZD 100176  
	    return true;
    }
    
    function chkGroupExists(gName)
    {
	    for(var i=0; i< document.frmManagegroup2.Group.options.length; i++)
	    {
		    if (gName.toLowerCase() == document.frmManagegroup2.Group.options(i).text.toLowerCase())
		    {	
			    return true;
		    }
	    }
    }
    
    // FB 2050 Starts
    function refreshIframe()
    {
    var iframeId = document.getElementById('ifrmMemberlist');
    iframeId.src = iframeId.src;
    }
    // FB 2050 Ends
    //ZD 100176 start
    function DataLoading(val) {
     if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
     else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
        }
        //ZD 100176 End

</script>

  <div id="tblViewDetails" style="display:none">
  </div>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
    <title>My Groups</title>
    <script type="text/javascript" src="inc/functions.js"></script>
</head>
<body>
    <form id="frmManagegroup2" runat="server" onsubmit="DataLoading('1')"> <%--ZD 100176--%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
   
     <input type="hidden" id="helpPage" value="73" />
        <table width="75%" align="center" cellpadding="4" border="0">
            <tr>
                <td align="center" colspan="4">
                    <h3>
                        <asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, ManageGroup2_lblHeader%>"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="width: 1168px">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr><div id="dataLoadingDIV" style="display:none" align="center" >
                    <img border='0' src='image/wait1.gif'  alt='Loading..' />
                 </div> <%--ZD 100678 End--%></tr><%--ZD 100176--%>
            <tr>
                <td colspan="4" align="left">
                    <span class="reqfldstarText">*</span>
                    <span class="reqfldText"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_RequiredField%>" runat="server"></asp:Literal></span>
                 </td>
            </tr>
            <tr>
                <td colspan="4">
                  <span class="subtitleblueblodtext" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_GroupInformati%>" runat="server"></asp:Literal></span>       
                </td>
            </tr>
            <tr>
                 <%--Window Dressing--%>
                <td align="left" style="width:16%;">
                   <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_GroupName%>" runat="server"></asp:Literal></span>
                    <span class="reqfldstarText">*</span>                   
                </td>
                <td colspan="2">
                     <asp:TextBox id="GroupName" runat="server" cssclass="altText" width="170"></asp:TextBox>
                     <asp:RequiredFieldValidator id="ReqFieldGName" text="<%$ Resources:WebResources, Required%>" errormessage="<%$ Resources:WebResources, Required%>" controltovalidate="GroupName" runat="server" setfocusonerror="true"></asp:RequiredFieldValidator>                     
                    <asp:RegularExpressionValidator id="regGName" controltovalidate="GroupName" display="dynamic" runat="server" validationgroup="Submit" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters2%>" validationexpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
                <%--Window Dressing--%>
                <td align="left">
                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_PublicGroup%>" runat="server"></asp:Literal></span>
                    <asp:CheckBox id="GroupPublic" runat="server"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                   <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_Description%>" runat="server"></asp:Literal></span>
                </td>
                <td colspan="3">
                    <asp:TextBox id="GroupDescription" textmode="MultiLine" width="200" rows="4" columns="15" runat="server" cssclass="altText"></asp:TextBox>                    
                    <asp:RegularExpressionValidator id="regGrpDescription" controltovalidate="GroupDescription" display="dynamic" runat="server" validationgroup="Submit" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters2%>" validationexpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4">
		          <span class="subtitleblueblodtext" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_MemberInformat%>" runat="server"></asp:Literal></span>
		        </td>
		     </tr>
		
		     <tr>
		       
		        <td colspan="4">
		            <table  cellpadding="2" cellspacing="0" width="100%"  height="95">
                      <tr>
                      <td></td>                        
                        <td  width="90%" bordercolor="#0000FF" align="left">
                          <table border="0" cellpadding="2" cellspacing="0" width="100%">
                           <%--Window Dressing start--%>
                            <tr class="tableHeader">
                              <td align="center" style="width:10%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_DELETE%>" runat="server"></asp:Literal></td><%--FB 2579--%>
                              <td align="center" style="width:50%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_NAME%>" runat="server"></asp:Literal></td>
                              <td align="center" style="width:40%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_EMAIL%>" runat="server"></asp:Literal></td>
                          <%--Window Dressing end--%>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                       <td height="10" align="left" valign="top" width="16%">
                 <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_Members%>" runat="server"></asp:Literal></span>
                </td>                     
                        <td bordercolor="#0000FF" align="center">
                          <table  cellpadding="0" cellspacing="0" width="100%" style="height:99">
                            <tr >
                              <td style="width:100%;height:100" valign="top" align="left">                              
                                <!--Added for Group2Member.asp Start-->                             
                                <iframe src="group2member.aspx?wintype=ifr" name="ifrmMemberlist" id="ifrmMemberlist" width="100%" height="100" style="vertical-align:top; "> <%--Edited for FF--%>
                                  <p><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_goto%>" runat="server"></asp:Literal><a href="group2member.aspx?wintype=ifr"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_Members%>" runat="server"></asp:Literal></a></p>
                                </iframe> 
                                <!--Added for Group2Member.asp End-->   
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>    
                    </table> 		            
		        </td>
		    </tr>
		    <tr >
                <td style="height:62" valign="top" align="left">
                  <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_Groups%>" runat="server"></asp:Literal></span>
                </td>
                <td style="width:30%">
                   <asp:ListBox ID="Group" CssClass="altText" style="scrollbar-shadow-color: #DEE3E7;" runat="server" SelectionMode="Multiple" Width="100%">
                    </asp:ListBox>   
                </td>
                <td>
                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_Clickongroup%>" runat="server"></asp:Literal><br /></span>
                    <asp:TextBox id="UsersStr" runat="server" width="0px" forecolor="transparent" backcolor="transparent" borderstyle="None" bordercolor="Transparent"></asp:TextBox>
                    <asp:TextBox id="PartysInfo" runat="server" width="0px" forecolor="Black" backcolor="transparent" borderstyle="None" bordercolor="Transparent"></asp:TextBox>
                </td>
                <td align="left" style="width:15%">
                <table> <%--Edited for FF--%>
                <tr>
                <td><%--ZD 100420--%>   
                    <button id="btnRemoveAll" runat="server" class="altMedium0BlueButtonFormat" style="width:160px" onclick="deleteAllParty();" ><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_RemoveAll%>" runat="server"></asp:Literal></button>
                    <%--<input type="button" name="Managegroup2Submit" value="Remove All" style="width:160px" class="altShortBlueButtonFormat" onclick="deleteAllParty();" lang="JavaScript" />--%>                    
                    </td>
                    </tr>
                <tr>
                <td>
                    <%--code changed for Softedge button--%><%--ZD 100420--%>   
                    <button id="btnAddressBook" runat="server" class="altMedium0BlueButtonFormat" style="width:160px" onclick="getYourOwnEmailList();" ><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_AddressBook%>" runat="server"></asp:Literal></button><%--ZD 100369--%> 
                    <%--<input type="button" id="btnAddressBook" name="Managegroup2Submit" value="myVRM Look Up" style="width:160px" class="altShortBlueButtonFormat" onclick="getYourOwnEmailList();" lang="JavaScript" />--%>
                 </td>
                </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                 <span class="subtitleblueblodtext" style="margin-left:-20px"><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_ConfirmYourGr%>" runat="server"></asp:Literal></span>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">                    
                    <%--code changed for Softedge button--%> <%--ZD 100420--%>
                    <button id="btnCancel" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:DataLoading('1'); window.location.replace('ManageGroup.aspx'); return false;" ><asp:Literal Text="<%$ Resources:WebResources, ManageGroup2_Cancel%>" runat="server"></asp:Literal></button>
                    <%--<button id="Managegroup2Submit" runat="server" class="altMedium0BlueButtonFormat" validationgroup="Submit" onserverclick="Managegroup2Submit_Click" onclick="frmManagegroup2_Validator();" >Submit</button>--%>
                    <%--<input type="button" name="btnCancel" onfocus="this.blur()" class="altShortBlueButtonFormat" value="Cancel" style="width:150px" onclick="javascript:DataLoading('1');window.location.replace('ManageGroup.aspx');" />--%><%--ZD 100176--%>
                    <asp:Button ID="Managegroup2Submit" runat="server" Text="<%$ Resources:WebResources, ManageGroup2_Submit%>" ValidationGroup="Submit"  style="width:150px" OnClick="Managegroup2Submit_Click" OnClientClick="javascript:return frmManagegroup2_Validator();" CssClass="altShortBlueButtonFormat" />
                </td>
            </tr>
         </table>
      </div>
    </form> 
    <script language="javascript">
        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
                if (document.getElementById("btnGoBack") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnGoBack").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };
        //ZD 100420 Start
        if (document.getElementById('Group') != null)
            document.getElementById('Group').setAttribute("onblur", "document.getElementById('btnRemoveAll').focus(); document.getElementById('btnRemoveAll').setAttribute('onfocus', '');");

        if (document.getElementById('btnCancel') != null)
            document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('Managegroup2Submit').focus(); document.getElementById('Managegroup2Submit').setAttribute('onfocus', '');");
    //ZD 100420 End
    
    </script>
    
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
